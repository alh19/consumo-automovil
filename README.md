# Consumo automovil

**Práctica programación Java_Ejercicio 1**

Escriba un programa para calcular el consumo de un automóvil.
Para ello el programa debe solicitar información sobre las tres últimas veces que se
repostó combustible.
De la primera solicitará el precio del litro del combustible, el total pagado en llenar el
depósito y el número de kilómetros que marcaba el cuentakilómetros.
De la segunda vez sólo solicitará el precio del litro del combustible y el total pagado en
llenar el depósito.
De la tercera vez, solicitará el valor que indicaba el cuentakilómetros.
Con estos datos debe calcular el consumo por cada 100 km y el coste por kilómetro.