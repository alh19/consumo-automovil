package pruebaconsumo;

import java.util.Scanner;

/**
 *
 * @author alh19
 */

public class PruebaConsumo {
    
    public static void main(String[] args) {
        
        Scanner entrada = new Scanner(System.in);
        Metodos func = new Metodos();
        
        float precio1, precio2;
        float coste1, coste2;
        float km1, km3;
        //float consumoMedio = 0, costeMedio = 0;
    
        System.out.print("Introduzca el precio por litro del primer repostaje: ");
        precio1 = entrada.nextFloat();
        System.out.print("Introduzca el coste total del primer repostaje: ");
        coste1 = entrada.nextInt();
        System.out.print("Introduzca el valor del cuentakilómetros en el primer repostaje: ");
        km1 = entrada.nextInt();
        
        System.out.print("Introduzca el precio por litro del segundo repostaje: ");
        precio2 = entrada.nextFloat();
        System.out.print("Introduzca el coste total del segundo repostaje: ");
        coste2 = entrada.nextInt();
        
        System.out.print("Introduzca el valor del cuentakilómetros en el tercer repostaje: ");
        km3 = entrada.nextInt();
        //consumoMedio = (((coste1/precio1)+(coste2/precio2))*100)/(km3-km1);
        //costeMedio = ((coste1 + coste2)/(km3-km1));
        System.out.printf("El consumo medio del automóvil es de: %.3f", func.consumoMedio(precio1, precio2, coste1, coste2, km1, km3), " litros cada 100 km\n");
        System.out.printf("El coste medio es de: %.3f", func.costeMedio(coste1, coste2, km1, km3), " por km");
    }
    
    
}
