/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package pruebaconsumo;

/**
 *
 * @author alber
 */
public class Metodos {
    public float consumoMedio(float precio1, float precio2, float coste1, float coste2, float km1, float km3){
            float resultadoConsumo = 0;
            resultadoConsumo = (((coste1/precio1)+(coste2/precio2))*100)/(km3-km1);
            return resultadoConsumo;
        }

        public float costeMedio(float coste1, float coste2, float km1, float km3){
            float resultadoCoste;
            float costeTotal = coste1 + coste2;
            float kmTotal = km3 - km1;
            resultadoCoste = costeTotal/kmTotal;
            
            return resultadoCoste;
        }
}
